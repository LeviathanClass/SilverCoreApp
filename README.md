# SilverCoreApp

#### 介绍
关于银核一代INS模块的最新上位机仓库（无需安装）

#### 安装教程
此仓库内为无需安装的版本，上位机依赖mingw73编译器编译，原则上兼容win10系统-x64架构的机器运行，而无需安装其他依赖包；
linux系统下的还在测试，请等待发布
    如需提供arm64架构下的linux版本，请联系开发者；

#### 使用说明
 **clone或下载zip后，双击exe文件即可打开使用。** 

 **在USB连接设备的情况下，波特率可以设置为任意值，而在UART转TTL连接设备的情况下，需要设置正确的波特率。** 
![设备连接](https://images.gitee.com/uploads/images/2020/0709/213745_786e06a9_1772556.png "屏幕截图.png")


 **波形显示，在开启对应接口的波形显示后，可以动态更新** 
![波形显示](https://images.gitee.com/uploads/images/2020/0709/211719_edb257c7_1772556.png "屏幕截图.png")

 **在连接设备后，可进行功能设置，具体设置请参考使用手册** 
![功能设置](https://images.gitee.com/uploads/images/2020/0709/214014_c1ad50d4_1772556.png "屏幕截图.png")

 **校准功能，目前仅开放加速度和角速度的平面校准，后续仍在开发中** 
![校准功能](https://images.gitee.com/uploads/images/2020/0709/214217_e085aec3_1772556.png "屏幕截图.png")

 **从远程服务器下载固件** 
![固件下载](https://images.gitee.com/uploads/images/2020/0709/213553_936fb341_1772556.png "屏幕截图.png")

从本地加载固件
![本地加载固件](https://images.gitee.com/uploads/images/2020/0709/214614_33cf899f_1772556.png "屏幕截图.png")